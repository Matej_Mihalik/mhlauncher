#include <gtk/gtk.h>
#include "defs.h"

void game_data_print(GameData* gameData) {
	if(gameData) {
		g_print("GAME DATA:\n");
		if(gameData->isUserLogged) {
			g_print("\tUser Logged: %s\n", *gameData->isUserLogged ? "true" : "false");
		}
		else {
			g_print("\tUser Logged: NULL\n");
		}
		if(gameData->isCaptchaActive) {
			g_print("\tCaptcha Active: %s\n", *gameData->isCaptchaActive ? "true" : "false");
		}
		else {
			g_print("\tCaptcha Active: NULL\n");
		}
		if(gameData->clientTimer) {
			g_print("\tClient Timer: %f\n", *gameData->clientTimer);
		}
		else {
			g_print("\tClient Timer: NULL\n");
		}
	}
	else {
		g_print("GAME DATA: NULL\n");
	}
}

void game_data_free(GameData* gameData) {
	if(gameData) {
		g_free(gameData->isUserLogged);
		g_free(gameData->isCaptchaActive);
		g_free(gameData->clientTimer);
		g_free(gameData);
	}
}

gboolean game_data_is_full(GameData* gameData) {
	return gameData && gameData->isUserLogged && gameData->isCaptchaActive && gameData->clientTimer;
}

void js_data_print(JSData* jsData) {
	if(jsData) {
		g_print("JS DATA:\n");
		g_print("\tMember: %s\n", jsData->member);
		game_data_print(jsData->gameData);
	}
	else {
		g_print("JS DATA: NULL\n");
	}
}

void js_data_free(JSData* jsData) {
	if(jsData) {
		g_free(jsData);
	}
}

gboolean js_data_is_full(JSData* jsData) {
	return jsData && game_data_is_full(jsData->gameData);
}

void error_print(gchar* message, GError* error) {
	g_print("%s\n", message);
	if(error) {
		g_print("ERROR:\n");
		g_print("\tDomain: %u\n", error->domain);
		g_print("\tCode: %i\n", error->code);
		g_print("\tMessage: %s\n", error->message);
	}
	else {
		g_print("ERROR: NULL\n");
	}
}

void error_free(GError* error) {
	if(error) {
		g_error_free(error);
	}
}

gboolean error_is_full(GError* error) {
	return error && error->domain && error->code && error->message;
}
