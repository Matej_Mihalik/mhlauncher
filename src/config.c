#include <gtk/gtk.h>

const guint REFRESH_RATE = 5;
const gfloat TURN_DURATION = 15*60;
const gchar* APP_VERSION = "1.0";
const gchar* APP_ID = "mhlauncher.desktop";
const gchar* ICON_ID = "mhlauncher";
const gchar* WINDOW_TITLE = "MouseHunt Launcher";
const gchar* BASE_URI = "https://www.mousehuntgame.com/";
const gchar* AJAX_PREFIX = "https://www.mousehuntgame.com/managers/ajax/turns";
const gchar* COOKIE_JAR_LOCATION = ".mhlauncher/cookie_jar";
const gchar* QUICKLIST_ITEM_LABEL = "Sound the Horn";
const gchar* SCRIPT_GET_IS_USER_LOGGED = "user.is_online;";
const gchar* SCRIPT_GET_IS_CAPTCHA_ACTIVE = "user.has_puzzle;";
const gchar* SCRIPT_GET_CLIENT_TIMER = "HuntersHorn.getSecondsRemaining();";
const gchar* SCRIPT_TAKE_TURN = "HuntersHorn.sound();";
const gint DEFAULT_WINDOW_WIDTH = 1000;
const gint DEFAULT_WINDOW_HEIGHT = 1200;

gint PREFERED_WINDOW_WIDTH = 0;
gint PREFERED_WINDOW_HEIGHT = 0;
