#include <gtk/gtk.h>
#include <unity.h>
#include "defs.h"
#include "config.h"
#include "window.h"

static UnityLauncherEntry* launcherEntry;
static DbusmenuMenuitem* quicklistItem;

static void launcher_set_count(GameData* gameData);
static void launcher_set_progress(GameData* gameData);
static void launcher_set_urgent(GameData* gameData);
static void launcher_set_quicklist(GameData* gameData);

void launcher_init() {
	launcherEntry = unity_launcher_entry_get_for_desktop_id(APP_ID);

	DbusmenuMenuitem* quicklist = dbusmenu_menuitem_new();
	unity_launcher_entry_set_quicklist(launcherEntry, quicklist);

	quicklistItem = dbusmenu_menuitem_new();
	dbusmenu_menuitem_property_set(quicklistItem, DBUSMENU_MENUITEM_PROP_LABEL, QUICKLIST_ITEM_LABEL);
	dbusmenu_menuitem_child_append(quicklist, quicklistItem);

	g_signal_connect(quicklistItem, DBUSMENU_MENUITEM_SIGNAL_ITEM_ACTIVATED, G_CALLBACK(window_take_turn), NULL);
}

void launcher_update(GameData* gameData) {
	launcher_set_count(gameData);
	launcher_set_progress(gameData);
	launcher_set_urgent(gameData);
	launcher_set_quicklist(gameData);
}

static void launcher_set_count(GameData* gameData) {
	gint64 timer = ceil(*gameData->clientTimer / 60);
	unity_launcher_entry_set_count(launcherEntry, timer);
	unity_launcher_entry_set_count_visible(launcherEntry, timer>0);
}

static void launcher_set_progress(GameData* gameData) {
	gdouble progress = 1 - *gameData->clientTimer / TURN_DURATION;
	unity_launcher_entry_set_progress(launcherEntry, progress);
	unity_launcher_entry_set_progress_visible(launcherEntry, progress<1);
}

static void launcher_set_urgent(GameData* gameData) {
	unity_launcher_entry_set_urgent(launcherEntry, *gameData->isUserLogged && !*gameData->isCaptchaActive && *gameData->clientTimer<=0);
}

static void launcher_set_quicklist(GameData* gameData) {
	dbusmenu_menuitem_property_set_bool(quicklistItem, DBUSMENU_MENUITEM_PROP_ENABLED, *gameData->isUserLogged && !*gameData->isCaptchaActive && *gameData->clientTimer<=0);
}
