#include <gtk/gtk.h>
#include <webkit2/webkit2.h>
#include <JavaScriptCore/JavaScript.h>
#include <json-glib/json-glib.h>
#include "defs.h"
#include "config.h"
#include "utils.h"
#include "launcher.h"
#include "notification.h"

static gboolean notifyLockCaptcha;
static gboolean notifyLockHunt;
static guint timeoutID;
static GtkWindow* window;
static WebKitWebView* webView;

static gint calculate_window_width();
static gint calculate_window_height();
static void window_destroy(GtkWidget* object, gpointer userData);
static void web_view_close(WebKitWebView* webView, gpointer userData);
static void request_started(WebKitWebView* web_view, WebKitWebResource* resource, WebKitURIRequest* request, gpointer userData);
static void request_finished(WebKitWebResource* resource, gpointer userData);
static void request_read(GObject* object, GAsyncResult* result, gpointer userData);
static void request_display(JsonReader* reader);
static void timeout_start(WebKitWebView* webView, WebKitLoadEvent loadEvent, gpointer userData);
static gboolean game_data_load(gpointer userData);
static void game_data_loaded(GObject* object, GAsyncResult* result, gpointer userData);
static void app_update(GameData* gameData);

void window_init() {
	notifyLockCaptcha = FALSE;
	notifyLockHunt = FALSE;
	timeoutID = 0;

	window = GTK_WINDOW(gtk_window_new(GTK_WINDOW_TOPLEVEL));
	gtk_window_set_default_size(window, calculate_window_width(), calculate_window_height());
	gtk_window_set_title(window, WINDOW_TITLE);

	webView = WEBKIT_WEB_VIEW(webkit_web_view_new());
	gtk_container_add(GTK_CONTAINER(window), GTK_WIDGET(webView));

	WebKitSettings* settings = webkit_web_view_get_settings(webView);
	webkit_settings_set_enable_javascript(settings, TRUE);
	webkit_settings_set_enable_plugins(settings, TRUE);

	WebKitWebContext* context = webkit_web_view_get_context(webView);
	WebKitCookieManager* cookieManager = webkit_web_context_get_cookie_manager(context);
	gchar* cookieJarPath = g_strconcat(g_get_home_dir(), "/", COOKIE_JAR_LOCATION, NULL);
	webkit_cookie_manager_set_persistent_storage(cookieManager, cookieJarPath, WEBKIT_COOKIE_PERSISTENT_STORAGE_TEXT);
	g_free(cookieJarPath);

	g_signal_connect(window, "destroy", G_CALLBACK(window_destroy), NULL);
	g_signal_connect(webView, "close", G_CALLBACK(web_view_close), NULL);
	g_signal_connect(webView, "resource-load-started", G_CALLBACK(request_started), NULL);
	g_signal_connect(webView, "load-changed", G_CALLBACK(timeout_start), NULL);

	launcher_init();
	notification_init();
}

void window_show() {
	gtk_widget_grab_focus(GTK_WIDGET(webView));
	gtk_widget_show_all(GTK_WIDGET(window));
}

void window_load() {
	webkit_web_view_load_uri(webView, BASE_URI);
}

void window_take_turn() {
	webkit_web_view_run_javascript(webView, SCRIPT_TAKE_TURN, NULL, NULL, NULL);
}

static gint calculate_window_width() {
	return PREFERED_WINDOW_WIDTH>0 ? PREFERED_WINDOW_WIDTH : DEFAULT_WINDOW_WIDTH;
}

static gint calculate_window_height() {
	return PREFERED_WINDOW_HEIGHT>0 ? PREFERED_WINDOW_HEIGHT : DEFAULT_WINDOW_HEIGHT;
}

static void window_destroy(GtkWidget* object, gpointer userData) {
	notification_destroy();
	gtk_main_quit();
}

static void web_view_close(WebKitWebView* webView, gpointer userData) {
	gtk_widget_destroy(GTK_WIDGET(window));
}

static void request_started(WebKitWebView* web_view, WebKitWebResource* resource, WebKitURIRequest* request, gpointer userData) {
	if(g_str_has_prefix(webkit_web_resource_get_uri(resource), AJAX_PREFIX)) {
		g_signal_connect(resource, "finished", G_CALLBACK(request_finished), NULL);
	}
}

static void request_finished(WebKitWebResource* resource, gpointer userData) {
	webkit_web_resource_get_data (resource, NULL, request_read, NULL);
}

static void request_read(GObject* object, GAsyncResult* result, gpointer userData) {
	gsize dataLength;
	GError* error = NULL;
	guchar* response = webkit_web_resource_get_data_finish(WEBKIT_WEB_RESOURCE(object), result, &dataLength, &error);

	if(!response) {
		error_print("Error getting response for web resource!", error);
		error_free(error);
		return;
	}

	JsonParser* parser = json_parser_new();
	gboolean sucess = json_parser_load_from_data(parser, response, dataLength, &error);

	if(!sucess) {
		error_print("Error getting parser for response!", error);
		error_free(error);
		return;
	}

	JsonReader* reader = json_reader_new(json_parser_get_root(parser));

	json_reader_read_member(reader, "journal_markup");
	gint messageCount = json_reader_count_elements(reader);

	gint i;
	for(i=0; i<messageCount; i++) {
		json_reader_read_element(reader, i);
		request_display(reader);
		json_reader_end_element(reader);
	}

	json_reader_end_member(reader);

	g_object_unref(reader);
	g_object_unref(parser);
}

static void request_display(JsonReader* reader) {
	gboolean isHuntResult = false;
	json_reader_read_member(reader, "publish_data");
	json_reader_read_member(reader, "attachment");
	json_reader_read_member(reader, "name");
	gchar* title = g_strdup(json_reader_get_string_value(reader));
	json_reader_end_member(reader);
	json_reader_read_member(reader, "description");
	gchar* text = g_strdup(json_reader_get_string_value(reader));
	json_reader_end_member(reader);
	json_reader_end_member(reader);
	json_reader_end_member(reader);
	gchar* image = NULL;

	json_reader_read_member(reader, "render_data");
	json_reader_read_member(reader, "css_class");
	gchar* classString = g_strdup(json_reader_get_string_value(reader));
	gchar** classTokens = g_strsplit_set(classString, " ", 0);
	json_reader_end_member(reader);

	if(g_strv_contains((const gchar**) classTokens, "attractionfailure")) {
		isHuntResult = true;
		image = g_strdup("https://mhcdn.scdn4.secure.raxcdn.com/images/ui/journal/steal.gif");
	}
	else if(g_strv_contains((const gchar**) classTokens, "attractionfailurestale")) {
		isHuntResult = true;
		image = g_strdup("https://mhcdn.scdn4.secure.raxcdn.com/images/ui/journal/stale.gif");
	}
	else if(g_strv_contains((const gchar**) classTokens, "catchfailure")) {
		isHuntResult = true;
		image = g_strdup("https://mhcdn.scdn4.secure.raxcdn.com/images/ui/journal/steal.gif");
	}
	else if(g_strv_contains((const gchar**) classTokens, "catchfailuredamage")) {
		isHuntResult = true;
		image = g_strdup("https://mhcdn.scdn4.secure.raxcdn.com/images/ui/journal/pillage.gif");
	}
	else if(g_strv_contains((const gchar**) classTokens, "catchsuccess")) {
		isHuntResult = true;
		json_reader_read_member(reader, "image");
		json_reader_read_member(reader, "val");
		image = g_strdup(json_reader_get_string_value(reader));
		json_reader_end_member(reader);
		json_reader_end_member(reader);
	}
	else if(g_strv_contains((const gchar**) classTokens, "catchsuccessloot")) {
		isHuntResult = true;
		json_reader_read_member(reader, "image");
		json_reader_read_member(reader, "val");
		image = g_strdup(json_reader_get_string_value(reader));
		json_reader_end_member(reader);
		json_reader_end_member(reader);
	}

	g_free(classString);
	g_strfreev(classTokens);
	json_reader_end_member(reader);

	if(isHuntResult) {
		notification_show(title, text, image);
	}

	g_free(title);
	g_free(text);
	g_free(image);
}

static void timeout_start(WebKitWebView* webView, WebKitLoadEvent loadEvent, gpointer userData) {
	if(timeoutID==0 && loadEvent==WEBKIT_LOAD_FINISHED && g_strcmp0(webkit_web_view_get_uri(webView), BASE_URI)==0) {
		game_data_load(NULL);
		timeoutID = g_timeout_add_seconds(REFRESH_RATE, game_data_load, NULL);
	}
}

static gboolean game_data_load(gpointer userData) {
	GameData* gameData = g_malloc(sizeof(GameData));
	gameData->isUserLogged = NULL;
	gameData->isCaptchaActive = NULL;
	gameData->clientTimer = NULL;

	JSData* isUserLoggedJSData = g_malloc(sizeof(JSData));
	isUserLoggedJSData->member = "isUserLogged";
	isUserLoggedJSData->gameData = gameData;
	webkit_web_view_run_javascript(webView, SCRIPT_GET_IS_USER_LOGGED, NULL, game_data_loaded, isUserLoggedJSData);

	JSData* isCaptchaActiveJSData = g_malloc(sizeof(JSData));
	isCaptchaActiveJSData->member = "isCaptchaActive";
	isCaptchaActiveJSData->gameData = gameData;
	webkit_web_view_run_javascript(webView, SCRIPT_GET_IS_CAPTCHA_ACTIVE, NULL, game_data_loaded, isCaptchaActiveJSData);

	JSData* clientTimerJSData = g_malloc(sizeof(JSData));
	clientTimerJSData->member = "clientTimer";
	clientTimerJSData->gameData = gameData;
	webkit_web_view_run_javascript(webView, SCRIPT_GET_CLIENT_TIMER, NULL, game_data_loaded, clientTimerJSData);

	return G_SOURCE_CONTINUE;
}

static void game_data_loaded(GObject* object, GAsyncResult* result, gpointer userData) {
	JSGlobalContextRef jsContext = NULL;
	JSValueRef jsValue = NULL;
	JSData* jsData = (JSData*) userData;

	GError* error = NULL;
	WebKitJavascriptResult* jsResult = webkit_web_view_run_javascript_finish(WEBKIT_WEB_VIEW(object), result, &error);

	if(jsResult) {
		jsContext = webkit_javascript_result_get_global_context(jsResult);
		jsValue = webkit_javascript_result_get_value(jsResult);
	}
	else {
		// error_print("Error getting javascript result!", error);
		error_free(error);
	}

	if(g_strcmp0(jsData->member, "isUserLogged")==0) {
		gboolean value = FALSE;
		jsData->gameData->isUserLogged = g_malloc(sizeof(gboolean));

		if(jsResult) {
			value = JSValueToBoolean(jsContext, jsValue);
		}

		*jsData->gameData->isUserLogged = value;
	}
	else if(g_strcmp0(jsData->member, "isCaptchaActive")==0) {
		gboolean value = FALSE;
		jsData->gameData->isCaptchaActive = g_malloc(sizeof(gboolean));

		if(jsResult) {
			value = JSValueToBoolean(jsContext, jsValue);
		}

		*jsData->gameData->isCaptchaActive = value;
	}
	else if(g_strcmp0(jsData->member, "clientTimer")==0) {
		gfloat value = 0;
		jsData->gameData->clientTimer = g_malloc(sizeof(gfloat));

		if(jsResult) {
			value = JSValueToNumber(jsContext, jsValue, NULL);
		}

		*jsData->gameData->clientTimer = value;
	}

	if(game_data_is_full(jsData->gameData)) {
		app_update(jsData->gameData);
		game_data_free(jsData->gameData);
	}

	js_data_free(jsData);
	if(jsResult) {
		webkit_javascript_result_unref(jsResult);
	}
}

static void app_update(GameData* gameData) {
	if(*gameData->isUserLogged) {
		if(*gameData->isCaptchaActive && !notifyLockCaptcha) {
			notification_show("King's Reward!", "You must claim a King's Reward before the hunt can continue.", NULL);
		}
		else if(*gameData->clientTimer==0 && !notifyLockHunt) {
			notification_show("Ready to Hunt!", "Sound the Hunter's Horn to continue the hunt.", NULL);
		}

		notifyLockCaptcha = *gameData->isCaptchaActive;
		notifyLockHunt = *gameData->clientTimer==0;
	}

	launcher_update(gameData);
}
