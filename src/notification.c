#include <gtk/gtk.h>
#include <libnotify/notification.h>
#include "config.h"

void notification_init() {
	notify_init(APP_ID);
}

void notification_destroy() {
	notify_uninit();
}

void notification_show(gchar* title, gchar* message, gchar* image) {
	GError* error = NULL;
	NotifyNotification* notification = notify_notification_new(title, message, ICON_ID);

	if(image) {
		GFile* imageFile = g_file_new_for_uri(image);
		GInputStream* imageStream = G_INPUT_STREAM(g_file_read(imageFile, NULL, &error));

		if(imageStream) {
			GdkPixbuf* pixbuf = gdk_pixbuf_new_from_stream(imageStream, NULL, &error);

			if(pixbuf) {
				notify_notification_set_image_from_pixbuf(notification, pixbuf);

				g_object_unref(pixbuf);
			}
			else {
				error_print("Error getting pixbuf from stream!", error);
				error_free(error);
			}

			g_object_unref(imageStream);
		}
		else {
			error_print("Error gettings stream from URI!", error);
			error_free(error);
		}

		g_object_unref(imageFile);
	}


	gboolean sucess = notify_notification_show(notification, &error);

	if(!sucess) {
		error_print("Error displaying notification!", error);
		error_free(error);
	}
}
