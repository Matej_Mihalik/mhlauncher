#include <stdlib.h>
#include <gtk/gtk.h>
#include "config.h"
#include "window.h"

gboolean print_version_and_exit(const gchar* option_name, const gchar* value, gpointer data, GError** error);

int main(gint argc, gchar** argv) {
	gtk_init(&argc, &argv);

	GError* error = NULL;
	GOptionContext* context = g_option_context_new(NULL);
	GOptionEntry supportedArguments[] = {
		{"version", 'v', G_OPTION_FLAG_NO_ARG, G_OPTION_ARG_CALLBACK, print_version_and_exit, "Print app version and exit", NULL},
		{"width", 'w', G_OPTION_FLAG_NONE, G_OPTION_ARG_INT, &PREFERED_WINDOW_WIDTH, "Set default window width", "N"},
		{"height", 'h', G_OPTION_FLAG_NONE, G_OPTION_ARG_INT, &PREFERED_WINDOW_HEIGHT, "Set default window height", "M"},
		{NULL}
	};
	g_option_context_add_main_entries(context, supportedArguments, NULL);

	if(!g_option_context_parse(context, &argc, &argv, &error)) {
		error_print("Error parsing input arguments!", error);
		error_free(error);
		return EXIT_FAILURE;
	}

	window_init();
	window_show();
	window_load();

	gtk_main();

	return EXIT_SUCCESS;
}

gboolean print_version_and_exit(const gchar* option_name, const gchar* value, gpointer data, GError** error) {
	g_print("MouseHunt Game Launcher %s\n", APP_VERSION);
	exit(EXIT_SUCCESS);
	return TRUE;
}
