APPNAME = mhlauncher
PWD = $(shell echo $$PWD)
HOME = $(shell echo $$HOME)
USER = $(shell echo $$LOGNAME)

all: mhlauncher;

mhlauncher:
	gcc src/*.c -o $(APPNAME) -I headers -lm $$(pkg-config --cflags gtk+-3.0 --libs gtk+-3.0 webkit2gtk-4.0 json-glib-1.0 unity libnotify);

install:
	ln -s $(PWD)/$(APPNAME) /usr/local/bin/;
	cp icons/48/$(APPNAME).png /usr/share/icons/hicolor/48x48/apps/;
	cp icons/256/$(APPNAME).png /usr/share/icons/hicolor/256x256/apps/;
	cp $(APPNAME).desktop /usr/share/applications/;
	mkdir $(HOME)/.$(APPNAME)/;
	chown $(USER) $(HOME)/.$(APPNAME)/;
	update-icon-caches /usr/share/icons/hicolor/;

clean:
	rm -f $(APPNAME);

uninstall:
	rm -f /usr/local/bin/$(APPNAME);
	rm -f /usr/share/icons/hicolor/48x48/apps/$(APPNAME).png;
	rm -f /usr/share/icons/hicolor/256x256/apps/$(APPNAME).png;
	rm -f /usr/share/applications/$(APPNAME).desktop;
	rm -rf $(HOME)/.$(APPNAME)/;
