#ifndef MHL_LAUNCHER
#define MHL_LAUNCHER

	#include "defs.h"

	void launcher_init();
	void launcher_update(GameData* gameData);

#endif
