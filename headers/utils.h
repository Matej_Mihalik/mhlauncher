#ifndef MHL_UTILS
#define MHL_UTILS

	#include <gtk/gtk.h>
	#include "defs.h"

	void game_data_print(GameData* gameData);
	void game_data_free(GameData* gameData);
	gboolean game_data_is_full(GameData* gameData);
	void js_data_print(JSData* jsData);
	void js_data_free(JSData* jsData);
	gboolean js_data_is_full(JSData* jsData);
	void error_print(gchar* message, GError* error);

#endif
