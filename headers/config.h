#ifndef MHL_CONFIG
#define MHL_CONFIG

	#include <gtk/gtk.h>

	extern const guint REFRESH_RATE;
	extern const gfloat TURN_DURATION;
	extern const gchar* APP_VERSION;
	extern const gchar* APP_ID;
	extern const gchar* ICON_ID;
	extern const gchar* WINDOW_TITLE;
	extern const gchar* BASE_URI;
	extern const gchar* AJAX_PREFIX;
	extern const gchar* COOKIE_JAR_LOCATION;
	extern const gchar* QUICKLIST_ITEM_LABEL;
	extern const gchar* SCRIPT_GET_IS_USER_LOGGED;
	extern const gchar* SCRIPT_GET_IS_CAPTCHA_ACTIVE;
	extern const gchar* SCRIPT_GET_CLIENT_TIMER;
	extern const gchar* SCRIPT_TAKE_TURN;
	extern const gint DEFAULT_WINDOW_WIDTH;
	extern const gint DEFAULT_WINDOW_HEIGHT;

	extern gint PREFERED_WINDOW_WIDTH;
	extern gint PREFERED_WINDOW_HEIGHT;

#endif
