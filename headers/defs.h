#ifndef MHL_DEFS
#define MHL_DEFS

	#include <gtk/gtk.h>

	typedef struct {
		gboolean* isUserLogged;
		gboolean* isCaptchaActive;
		gfloat* clientTimer;
	} GameData;

	typedef struct {
		gchar* member;
		GameData* gameData;
	} JSData;

#endif
