#ifndef MHL_NOTIFICATION
#define MHL_NOTIFICATION

	#include <gtk/gtk.h>

	void notification_init();
	void notification_destroy();
	void notification_show(gchar* title, gchar* message, gchar* image);

#endif