#ifndef MHL_WINDOW
#define MHL_WINDOW

	void window_init();
	void window_show();
	void window_load();
	void window_take_turn();

#endif
