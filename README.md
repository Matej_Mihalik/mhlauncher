# MouseHunt Launcher

## A C-based webkit wrapper around the MouseHunt game website made for systems running Ubuntu.
## Integrates with unity launcher and notification systems, to provide basic game interactions and info without having to shift focus from other important stuff - like work :)